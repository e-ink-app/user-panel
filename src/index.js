import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import * as firebase from 'firebase';
import * as serviceWorker from './serviceWorker';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBTLcWHzbVgNV65ZqK4KXSwQgteurED_jk",
    authDomain: "e-ink-app-d9474.firebaseapp.com",
    databaseURL: "https://e-ink-app-d9474.firebaseio.com",
    projectId: "e-ink-app-d9474",
    storageBucket: "e-ink-app-d9474.appspot.com",
    messagingSenderId: "661416515335"
  };
firebase.initializeApp(config);
const db = firebase.firestore();

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

export {db}