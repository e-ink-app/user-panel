import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Quote from '../Quote/Quote';
import Notes from '../Notes/Notes';
import Todo from '../Todo/Todo';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid className="grid" container spacing={16}>
          <Grid item md={4} sm={6} xs={12}>
            <Notes></Notes>
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <Todo></Todo>
          </Grid>
          <Grid item md={4} sm={6} xs={12}>
            <Quote></Quote>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
