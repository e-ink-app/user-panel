import React, { Component } from 'react';
import './Notes.css';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import * as firebase from 'firebase';
import { db } from '../..';

class Notes extends Component {
    constructor(props) {
        super(props);

        this.submitNotes = this.submitNotes.bind(this);
    }

    submitNotes(e) {
        e.preventDefault();
        const {notes} = e.target.elements
        // const notesref = firebase.database().ref().child('notes');
        let payload = notes.value.replace(/\n/g,'@')
        // notesref.set(payload);
        db.collection('common').doc('text').set({
          value: payload
        }).then(() => {
          alert('Posted!');
        })
    }
    
    render() {
        return (
            <Card>
              <CardHeader title="Notes" />
              <CardContent>
                <form onSubmit={this.submitNotes}>
                    <p>
                        Enter the value below that you want to show in the 'notes' section
                    </p>
                    <TextField multiline fullWidth id="notes-input" name="notes" placeholder="Enter notes here..." />
                    <Button className="submit-button" variant="contained" type="submit" color="primary">Send</Button>
                </form>
              </CardContent>
            </Card>
        );
    }
}

export default Notes;