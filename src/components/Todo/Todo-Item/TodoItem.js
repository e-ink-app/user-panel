import React, { Component } from 'react';
import './TodoItem.css';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import * as firebase from 'firebase';

class TodoItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            key: props.key,
        }

        this.deleteItem = this.deleteItem.bind(this);
    }

    deleteItem() {
        const itemRef = firebase.database().ref().child(`/checklist/${this.props.firekey}`);
        itemRef.remove();
    }
    
    render() {
        return (
            <li>
                {this.props.name} <DeleteForeverOutlinedIcon className="mouse-pointer" onClick={this.deleteItem} />
            </li>
        );
    }
}

export default TodoItem;