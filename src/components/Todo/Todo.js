import React, { Component } from 'react';
import './Todo.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import * as firebase from 'firebase';
import TodoItem from './Todo-Item/TodoItem';
class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
        }
        this.addTodo = this.addTodo.bind(this);
    }

    componentDidMount() {
        const todoref = firebase.database().ref().child('checklist');
        todoref.on('value', snap => {
            let finalArray = [];
            let raw = snap.val();
            console.log(raw);
            for(let key in raw) {
                if (raw.hasOwnProperty(key)) {
                    let preparedValues = [key, raw[key].item];
                    finalArray.push(preparedValues);
                }
            }
            this.setState({
                list: finalArray
            })
        });
    }

    addTodo(e) {
        e.preventDefault();
        const {todo} = e.target.elements;
        const todoref = firebase.database().ref().child('checklist');
        todoref.push({
            item: todo.value,
        });
        document.getElementById("todo-add-form").reset();
    }

    render() {
        return (
            <Card>
                <CardHeader title="To-do list"/>
                <CardContent>
                    <form onSubmit={this.addTodo} id="todo-add-form">
                        <ul>
                            {this.state.list.map((e, i) => {
                                return <TodoItem key={e[0]} firekey={e[0]} name={e[1]}></TodoItem>
                            })}
                        </ul>
                        <TextField fullWidth name="todo" placeholder="Add new item" />
                        <Button variant="contained" className="submit-button" type="submit" color="primary">Add</Button>
                    </form>
                </CardContent>
            </Card>
        );
    }
}

export default Todo;